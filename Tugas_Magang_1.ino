/* 
 * Nama : Muhammad Nabil Akbar Mustafa
 * NiM : 21/481679/PA/20981
 */

#define pinmotor1 4 // mendeklarasikan pin 4 (pin digital) pada arduino
#define pinmotor2 5 // mendeklarasikan pin 5 (pin PWM) pada arduino

int speed; // deklarasi variable bernama speed  

void setup(){
  pinMode(pinmotor1 , OUTPUT); // pinmode untuk setting pinmotor1 sebagai output
  pinMode(pinmotor2 , OUTPUT); // pinmode untuk setting pinmotor2 sebagai output
  
  Serial.begin(9600); // serial.begin untuk mengatur kecepatan komunikasi dengan 9600 bit per detik
  Serial.println ("Atur kecepatan dari 0 sampai 255"); // menampilkan "Atur kecepatan dari 0 sampai 255" di dalam serial monitor agar dimasukkan angka 0-255 oleh user
}

void loop(){
  if(Serial.available()>0){ 
    speed = Serial.parseInt(); // memasukkan data integer dari serial monitor ke variable speed
  }
  analogWrite(pinmotor2, speed); // pinmotor2 karena pin pwm diberikan sesuai nilai yang ada di dalam variable speed
  digitalWrite(pinmotor1, LOW); // pinmotor1 karena pin digital diberikan low
  Serial.println(speed); // menampilkan nilai dalam variable speed ke serial monitor (kecepatan dari motor dc)
  delay(1000); // membuat delay selama 1 detik
}
